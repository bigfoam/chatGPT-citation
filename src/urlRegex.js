export const chatGPTPageUrlRegex = /https:\/\/chat.openai.com\/.*/;

export const apiGetConversation =
  /https:\/\/chat.openai.com\/backend-api\/conversation\/([^?]+)/;
export const apiGetConversations =
  /https:\/\/chat.openai.com\/backend-api\/conversations?offset=[\d]+&limit=[\d]+/;

export const apiGetModel = /https:\/\/chat.openai.com\/backend-api\/models/;
export const apiGetSession = /https:\/\/chat.openai.com\/api\/auth\/session/;

export const apiPostConversation =
  /https:\/\/chat.openai.com\/backend-api\/conversation$/;
export const apiPostModeration =
  /https:\/\/chat.openai.com\/backend-api\/moderations$/;
