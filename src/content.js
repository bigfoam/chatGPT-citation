document.body.addEventListener(
  'click',
  async (event) => {
    if (!event.target.matches('.generate-citation')) return;
    const position =
      Array.from(document.getElementsByClassName('generate-citation')).indexOf(
        event.target
      ) + 1; // start from 1
    const citationData = await chrome.runtime.sendMessage(
      `get-citation-${position}}`
    );
    // Copy citation data to clipboard
    await navigator.clipboard.writeText(JSON.stringify(citationData));
    alert('Citation generated and copied to clipboard');
    // Focus on the message input box and also lose focus on the button
    document.querySelector('textarea[tabIndex="0"]').focus();
  },
  false
);

const injectGenerateCitationButton = (parentNode) => {
  const grBtn = document.createElement('button');
  grBtn.className = 'btn btn-primary generate-citation';
  grBtn.innerHTML = 'Cite This Answer';
  parentNode.append(grBtn);
};

const observer = new MutationObserver((mutationList) => {
  const actionBtnContainerSelector = '.text-gray-400.flex.self-end';
  const hoverVisibleBtnSelector = '.md\\:invisible.md\\:group-hover\\:visible';

  mutationList.forEach((mutation) => {
    /* mutation.type
     * childList: One or more children have been added to and/or removed from the tree.
     * attributes: An attribute value changed on the element in mutation.target.
     */
    if (mutation.type === 'childList') {
      mutation.addedNodes.forEach((addedNode) => {
        if (addedNode.nodeType === 1) {
          // an element node
          addedNode
            .querySelectorAll(actionBtnContainerSelector)
            .forEach((btnContainer) => {
              /*
               * The btnContainer of user input has 1 button, while the btnContainer of ChatGPT bot response has 2 buttons.
               * But it's not safe to simply check if btnContainer.childElementCount === 2
               * because other chrome extensions may have added buttons to the btnContainer.
               * Therefore, best practice is to check if the btnContainer NOT has a button with class `md:invisible md:group-hover:visible`
               */
              const hoverVisibleBtn = btnContainer.querySelector(
                hoverVisibleBtnSelector
              );
              if (!hoverVisibleBtn) {
                injectGenerateCitationButton(btnContainer);
              }
            });
        }
      });
    }
  });
});

observer.observe(document.body, {
  childList: true,
  subtree: true,
  attributes: false,
  characterData: false
});
