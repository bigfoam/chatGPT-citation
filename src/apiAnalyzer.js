export const API_EVENT_TYPES = {
  GET_CONVERSATION_HISTORY: 'GET_CONVERSATION_HISTORY',
  GET_SESSION_INFO: 'GET_SESSION_INFO',
  POST_NEW_CONVERSATION: 'POST_NEW_CONVERSATION',
  POST_MODERATION: 'POST_MODERATION'
};

export const analyzeApiResponse = ({ json, tabData, timestamp, type }) => {
  switch (type) {
    case API_EVENT_TYPES.GET_CONVERSATION_HISTORY: {
      const { title, mapping } = json;
      tabData.metaData.title = title;
      // Convert mapping to conversation array
      Object.values(mapping).forEach((item) => {
        if (item.message?.author?.role === 'user') {
          const convoItem = {
            id: item.message.id,
            create_time: item.message.create_time
              ? Math.round(item.message.create_time * 1000)
              : '',
            prompt: item.message.content, // { "content_type":"text", "parts":[string] }
            answers: []
          };
          if (Array.isArray(item.children)) {
            item.children.forEach((childId) => {
              if (mapping[childId]?.message) {
                convoItem.answers.push({
                  id: mapping[childId].message.id,
                  create_time: mapping[childId].message.create_time
                    ? Math.round(mapping[childId].message.create_time * 1000)
                    : '',
                  content: mapping[childId].message.content,
                  model: mapping[childId].message.metadata?.model_slug
                });
              }
            });
          }
          tabData.conversation.push(convoItem);
        }
      });
      // Sort conversation array by create_time in ascending order
      tabData.conversation.sort((a, b) => {
        return a.create_time - b.create_time;
      });
      break;
    }

    case API_EVENT_TYPES.GET_SESSION_INFO: {
      if (json.user) {
        const { email = '', id = '', name = '' } = json.user;
        tabData.metaData.email = email;
        tabData.metaData.userId = id;
        tabData.metaData.username = name;
      }
      break;
    }

    case API_EVENT_TYPES.POST_NEW_CONVERSATION: {
      const { messages, model } = json;
      const lastConvoItem =
        tabData.conversation[tabData.conversation.length - 1];
      // If user hit "Regenerate response" btn, we don't need to create a new conversation item
      if (messages[0] && lastConvoItem?.id !== messages[0].id) {
        tabData.conversation.push({
          id: messages[0].id,
          create_time: timestamp,
          model, // moderation request doesn't have the correct model
          prompt: messages[0].content,
          answers: []
        });
      }
      break;
    }

    case API_EVENT_TYPES.POST_MODERATION: {
      // currently input is always text
      const { input, message_id } = json;
      const lastConvoItem =
        tabData.conversation[tabData.conversation.length - 1];
      if (lastConvoItem?.id !== message_id) {
        const searchStrLength = 20;
        // Get the last {searchStrLength} characters of the prompt and search for it in the input
        // If found, the rest of input is assumed to be the answer to the prompt
        const searchStr = lastConvoItem.prompt?.parts?.[
          lastConvoItem.prompt?.parts?.length - 1
        ]?.slice(-1 * searchStrLength);
        const answer = input?.slice(
          input?.lastIndexOf(searchStr) + searchStr.length
        );
        lastConvoItem.answers.push({
          id: message_id,
          create_time: timestamp,
          content: { content_type: 'text', parts: [answer] },
          model: lastConvoItem.model // assume the model is the same
        });
      }
      break;
    }
  }
};

export default analyzeApiResponse;
