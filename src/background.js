/* eslint-disable no-prototype-builtins */
import {
  apiGetConversation,
  apiGetSession,
  apiPostConversation,
  apiPostModeration,
  chatGPTPageUrlRegex
} from './urlRegex.js';
import { analyzeApiResponse, API_EVENT_TYPES } from './apiAnalyzer.js';
// https://chromedevtools.github.io/devtools-protocol/
const requiredDebuggingProtocolVersion = '1.3';

(() => {
  const tabStorage = {};
  const createTabData = () => ({
    apiRequestIdsToType: {},
    conversation: [],
    metaData: {
      conversationId: '',
      email: '',
      title: '',
      userId: '',
      username: ''
    }
  });

  const allEventHandler = async (source, method, params) => {
    const timestamp = new Date().getTime();
    const { tabId } = source;
    if (!tabStorage.hasOwnProperty(tabId)) return;
    const tabData = tabStorage[tabId];
    const { apiRequestIdsToType } = tabData;

    // Mark the requests to monitor
    if (method === 'Network.requestWillBeSent') {
      const { method, url } = params.request;
      let type;
      // API request to monitor and analyze response
      if (apiGetConversation.test(url) && method === 'GET') {
        type = API_EVENT_TYPES.GET_CONVERSATION_HISTORY;
        const conversationId = url.match(apiGetConversation)[1];
        tabData.metaData.conversationId = conversationId;
        // clear conversation history
        tabData.conversation = [];
      } else if (apiGetSession.test(url) && method === 'GET') {
        type = API_EVENT_TYPES.GET_SESSION_INFO;
      }
      if (type) {
        apiRequestIdsToType[params.requestId] = type;
        return;
      }
      // API request to analyze request post data
      if (apiPostConversation.test(url) && method === 'POST') {
        type = API_EVENT_TYPES.POST_NEW_CONVERSATION;
      } else if (apiPostModeration.test(url) && method === 'POST') {
        type = API_EVENT_TYPES.POST_MODERATION;
      }
      if (type) {
        try {
          const { postData } = await chrome.debugger.sendCommand(
            { tabId },
            'Network.getRequestPostData',
            { requestId: params.requestId }
          );
          const json = JSON.parse(postData);
          analyzeApiResponse({
            json,
            tabData,
            timestamp,
            type
          });
        } catch (error) {
          console.error(`Failed to analyze request of api ${type}`, error);
        }
      }
    }

    // Analyze the response
    if (method === 'Network.responseReceived') {
      const type = apiRequestIdsToType[params.requestId];
      if (!type) return;
      delete apiRequestIdsToType[params.requestId];
      const response = await chrome.debugger.sendCommand(
        { tabId },
        'Network.getResponseBody',
        { requestId: params.requestId }
      );
      try {
        const json = JSON.parse(response.body);
        analyzeApiResponse({
          json,
          tabData,
          type
        });
      } catch (error) {
        console.error(`Failed to analyze response of api ${type}`, error);
      }
    }
  };

  // Listen to all events
  chrome.debugger.onEvent.addListener(allEventHandler);

  // Monitor ChatGPT tabs
  chrome.tabs.onUpdated.addListener(async (tabId, changeInfo) => {
    const { url: newUrl } = changeInfo || {};
    if (!newUrl) return;

    if (tabStorage.hasOwnProperty(tabId) && !chatGPTPageUrlRegex.test(newUrl)) {
      await chrome.debugger.detach({ tabId });
      delete tabStorage[tabId];
    } else if (
      !tabStorage.hasOwnProperty(tabId) &&
      chatGPTPageUrlRegex.test(newUrl)
    ) {
      tabStorage[tabId] = createTabData();
      await chrome.debugger.attach({ tabId }, requiredDebuggingProtocolVersion);
      // Enable the Network
      await chrome.debugger.sendCommand({ tabId }, 'Network.enable');
    }
  });

  chrome.tabs.onRemoved.addListener(async (tabId) => {
    if (tabStorage.hasOwnProperty(tabId)) {
      // no need to detach debugger because the tab was already removed
      // await chrome.debugger.detach({ tabId });
      delete tabStorage[tabId];
    }
  });

  // Listen to messages from content script
  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    const position = message.match(/get-citation-(\d+)/)?.[1];
    const tabData = tabStorage[sender.tab.id];

    if (!position || !tabData) sendResponse({});

    sendResponse({
      conversation: tabData.conversation?.slice(0, position),
      metaData: tabData.metaData
    });
  });
})();
